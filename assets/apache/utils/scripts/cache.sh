#!/bin/bash

P=/var/www/app/api/apps

cd $P/api

touch bootstrap.php.cache

cd ../

ln -s ./api/bootstrap.php.cache bootstrap.php.cache

cd auth

ln -s ../bootstrap.php.cache bootstrap.php.cache

