function _apt_sources() {
  say "Adding APT repositories"

  apt-get install python-software-properties --yes --force-yes 2>&1 | while read LINE; do echo $LINE &>> $LOG ; echo -n "."; done

  apt-add-repository ppa:chris-lea/node.js --yes &> /dev/null
  apt-add-repository ppa:ondrej/php5 --yes &> /dev/null
  apt-add-repository ppa:ondrej/apache2 --yes &> /dev/null
  apt-add-repository ppa:ondrej/mysql-5.5 --yes &> /dev/null
  echo "";
}



