#!/bin/bash
set -e
set -u

APPS="admin api assets frontend landing [REFRESH_AND_CONFIG_ENV] [CHECKALL] [STARTALL] [STOPALL] [DEPLOYALL] [quit]"
OPTIONS="start stop build check [deploy]"
ENVS="beta demo testing production development"
APPLS="admin api assets frontend landing"
CUR_DIR=`pwd`
LOG="$CUR_DIR/build.log"
REPO="git@bitbucket.org:pricepilot/pp-env.git"
USE_LOCAL=false
if [[ $* == *--local* ]]; then
    USE_LOCAL=true
fi
##check domains.

function deploy_server () {
    server=$1
    app=$2
    env=$3
    rene=""
    manfred="78.46.43.40"
    rene="88.198.57.204"

    ssh -e cmd production@manfred

}

function get_code () {
#logically function fetches code from pp-env master repo into environment fresh folder
#
    env=$1

        echo "Get fresh code..."

        if [ -d $env  ] ; then
            echo "Removing old $CUR_DIR/$env dir..."
            rm -rf $CUR_DIR/$env
        fi

        if $USE_LOCAL; then
          echo "Cloning local"
          git clone . -b master $env
        else
          git clone -b master $REPO $env
        fi

        echo "Code fresh. Should now be compiled and placeholders replaced with real values."

    echo

}

function get_devbox_code() {

    workdir="$HOME/work/pp"
    API="git@bitbucket.org:mimazoo/buksl-api.git"
    FE="git@bitbucket.org:mimazoo/pp-frontend-new.git"
    ASSETS="git@bitbucket.org:mimazoo/buksl-assets.git"
    LANDING="git@bitbucket.org:pricepilot/pp-landing.git"
    ADMIN="git@bitbucket.org:mimazoo/booksl-admin.git"
    #git clone $API $workdir/api/code
    #git clone $FE $workdir/frontend/code
    #git clone $ASSETS $workdir/assets/code
    #git clone $LANDING $workdir/landing/code
    #git clone $ADMIN $workdir/admin/code
    echo "Cloned all 5 apps in $workdir"

    #get code from our repos to home/work/pp/app
    #than build the box.
}

function selector() {
#main function which executes docker-compose action in selected environment and application.

    app=$2
    opts=$1
    env=$3
    e="$CUR_DIR/$env/$app"
    cfile=" "
    if [ "$env" = "development" ]; then
        cfile=$e/development.yml
    else
        cfile=$e/docker-compose.yml
    fi

    case "$opts" in
        check )
            cd $e && docker-compose -f $cfile -p $env$app ps ;;
        start )
            cd $e && docker-compose -f $cfile -p $env$app up -d ;;
        stop )
            cd $e && docker-compose -f $cfile -p $env$app stop ;;
        build )
	    cd $e && ls -d */ -1a|xargs -i bash -c 'date +%s%N > {}deploy_id' && docker-compose -f $cfile -p $env$app  build ;;
        rm )
            cd $e && docker-compose -f $cfile -p $env$app rm --force ;;
        "[deploy]" )
          _deploy $app $env ;;
   esac

   echo `date` $CUR_DIR >> $LOG
   echo Workdir.................. >> $LOG
   echo $CUR_DIR/$env/$app >> $LOG
   echo $e
}

function send_message () {
#function for sending custom message into Pricepilot room on HipChat

    token=1811e1b9e6b8e06094673a47a3314c
    roomid=1092993
    message=$1
    server=$2
    hrm="$CUR_DIR/utils/hipchat-room-message"
    echo $hrm
    echo "$message" | $hrm -t $token -r $roomid -n -f "$server"

}

function get_hostname() {
    hostname=`hostname`
    echo ${hostname^^}
}


#Replace all the placeholders [{VARIABLE_NAME}] with actual values
function parse_configs() {

    #set ports base per app
    declare -A portBase

    portBase['development']=5010
    portBase['testing']=5020
    portBase['beta']=5030
    portBase['demo']=5040
    portBase['production']=5050

    env=$1

    declare -A config

    #common config, override it using the custom config case if needed
    config[ENV]=$env
    config[ENV_DOMAIN_PREFIX]="$env."
    config[DOMAIN]='pricepilot.si'

    #common ports config per app
    pb=${portBase[$env]}
    config[ADMIN_PORT]=$(($pb+1));
    config[API_PORT]=$(($pb+2));
    config[ASSETS_PORT]=$(($pb+3));
    config[FRONTEND_PORT]=$(($pb+4));
    config[LANDING_PORT]=$(($pb+5));
    config[DB_PORT]=$(($pb));

    #custom config per environment
    case $env in
        'beta')
            ;;
        'demo')
            ;;
        'testing')
            ;;
        'development')
            config[ENV_DOMAIN_PREFIX]=''
            config[DOMAIN]='pricepilot.loc'
            ;;
        'production')
            config[DB_PORT]=3409
            config[ENV_DOMAIN_PREFIX]=''
            config[DOMAIN]='pricepilot.io'
            ;;
    esac

    for i in "${!config[@]}"
    do
      placeholder="\{\[$i\]\}"
      value=${config[$i]}
      grep -rl $placeholder "./$env"|xargs -r sed -i -r "s/$placeholder/$value/g"
      echo "Replaced all {[$i}] placeholders with value ${config[$i]}"
    done

    echo "Code for env $env is ready to be used."
}

function parse_configs_old() {
#function which edits VHosts and docker-compose.yml configs for selected environment [beta, demo, testing]
#auth, should not be renamed to api. Get name of app from somewhere else.
#add Dockerfile env rename in api

    env=$1
    app=tmp
    domain=pricepilot.si
    vhosts=`find $env -name *.conf`
    names="find $env -name *.conf | cut -d'/' -f6 | cut -d'.' -f1"
    ymls=`find $env -name docker-compose.yml`
    #name="find $env -name docker-compose.yml | cut -d'/' -f2"
    count=" "
    if [ $env = "beta" ]; then
        count=20
    elif [ $env = "demo" ]; then
        count=40
    elif [ $env = "testing" ]; then
        count=60
    else
        echo
    fi

    let "count += 1"

    if [ -f utils/locks/$env.cfg ] ; then
        echo "$env alreaddy configured"
        echo "to unlock delete ./utils/locks/env.cfg file"
        break;
    fi
    echo "Renaming ServerNames for $env"
    for i in $vhosts; do
        name=$(echo $i | cut -d'/' -f6 | cut -d'.' -f1)
        if [ "$name" = "landing" ]; then
            sed -i -r "s/(ServerName).*$/ServerName $env.$domain/g" $i
        fi
        sed -i -r "s/(production)/$env/g" $env/api/api_app/Dockerfile
        sed -i -r "s/(production)/$env/g" $env/frontend/frontend_app/Dockerfile
        sed -i -r "s/(production\.dist)/$env\.dist/g" $env/admin/admin_app/Dockerfile
        sed -i -r "s/(production)/$env/g" $env/api/docker-compose.yml
        sed -i -r "s/(production)/$env/g" $env/api/api_documents/Dockerfile
        sed -i -r "s/(production)/$env/g" $env/api/apache/utils/vhosts/docs.api.pricepilot.conf
        sed -i -r "s/(ServerName).*$/ServerName $env.$name.$domain/g" $i
        sed -i -r "s/(ServerAlias) (.*)$/\1\  $env.$name.$domain\2/g" $i
        #sed -i -r "s/(ProxyPass).*$/ProxyPass $env.$name.$domain/g" $i
    done
    echo "Updating ports for apps in $env"
    for y in $ymls; do
        yml=$(echo $y | cut -d'/' -f2)
        if [ "$yml" = "api" ]; then
             sed -i -r "s/(prt)/$count/g" $y
        else
            sed -i -r "s/(\-\ \")[0-9]+(\:[0-9]+\")/\1$count\2/g" $y
        fi
        let "count += 1"
        echo $count
    done
    echo "configured $env" > ./utils/locks/$env.cfg
}

function _deploy() {
#function for deploying an application in selected environment. If build of container fails, function completely exits so it does not end up in loop of recreating #and rebuilding container

        application=$1
        env=$2
        echo "Deploying $application..........................................................."
        selector build $application $env
        if [ $? -ne 0 ]; then
            echo "Smth wrong... Break!"
            break;
        fi
        echo "Build ok, now stop the old ones..."
        selector stop $application $env &&
        selector rm $application $env &&
        selector start $application $env &&
        send_message "Deployed ${application^^} in ${env^^} environment." $(echo $(get_hostname))

}

function main () {

  select en in $ENVS; do

    if [ $en = "development" ]; then
      get_devbox_code;
    fi

  	select app in $APPS; do
            if [ "$app" = "admin" ]; then
            echo Doing Admin
            select opt in $OPTIONS; do
                selector $opt $app $en
                break;
          done

          elif [ "$app" = "api" ]; then
             select opt in $OPTIONS; do
               selector $opt $app $en
               break;
          done

    	 elif [ "$app" = "assets" ]; then
         		 select opt in $OPTIONS; do
             	 selector $opt $app $en
                break;
       	 done

    	 elif [ "$app" = "frontend" ]; then
          	select opt in $OPTIONS; do
             	 selector $opt $app $en
                break;
       	 done

    	 elif [ "$app" = "landing" ]; then
       	 select opt in $OPTIONS; do
              selector $opt $app $en
              break;
       	 done
       elif [ "$app" = "[REFRESH_AND_CONFIG_ENV]" ]; then
             get_code $en
             echo $en
             parse_configs $en
             break;

    	 elif [ "$app" = "[DEPLOYALL]" ]; then
       	 _deploy api $en
       	 _deploy assets $en
       	 _deploy admin $en
       	 _deploy frontend $en
       	 _deploy landing $en ;
       	 send_message "Deployed API ASSETS ADMIN FRONTEND LANDING" $(echo $(get_hostname))
          break;
       elif [ "$app" = "[CHECKALL]" ]; then
           selector check admin $en; selector check api $en; selector check assets $en; selector check frontend $en; selector check landing $en
           break;
       elif [ "$app" = "[STARTALL]" ]; then
          selector start admin $en; selector start api $en; selector start assets $en; selector start frontend $en; selector start landing $en
          break;
       elif [ "$app" = "[STOPALL]" ]; then
          selector stop admin $en; selector stop api $en; selector stop assets $en; selector stop frontend $en; selector stop landing $en
          break;
    	 else
   	       echo "none"
            break;
    	 fi
      done
  done

}

#echo $(get_hostname)
#send_message "test"
#get_hostname

main;
