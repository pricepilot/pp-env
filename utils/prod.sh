#!/bin/bash
#set -e
#set -u

SSH_API="git@bitbucket.org:mimazoo/buksl-api.git"
SSH_FE="git@bitbucket.org:mimazoo/pp-frontend-new.git"
SSH_ASSETS="git@bitbucket.org:mimazoo/buksl-assets.git"
SSH_LANDING="git@bitbucket.org:pricepilot/pp-landing.git"
SSH_ADMIN="git@bitbucket.org:mimazoo/booksl-admin.git"

HTTPS_API="https://Janz86@bitbucket.org/mimazoo/buksl-api.git"
HTTPS_FE="https://Janz86@bitbucket.org/mimazoo/pp-frontend-new.git"
HTTPS_ASSETS="https://Janz86@bitbucket.org/mimazoo/buksl-assets.git"
HTTPS_LANDING="https://Janz86@bitbucket.org/pricepilot/pp-landing.git"
HTTPS_ADMIN="https://Janz86@bitbucket.org/mimazoo/booksl-admin.git"

DOCKERUI="docker run -d -p 9000:9000 --privileged -v /var/run/docker.sock:/var/run/docker.sock dockerui/dockerui"

MLAND="containers/production/api_app/repos/landing"
MFE="containers/production/frontend_app/repos/frontend"
MASSETS="containers/production/api_app/repos/assets"
MAPI="containers/production/api_app/repos/pp-api"
MADMIN="containers/production/admin_app/repos/admin"

DEV_IN_HOSTS=" echo `cat /etc/hosts | grep api | cut -d' ' -f1` "
ARG="$1"
CUR_DIR=`pwd`

echo $ARG

if [ "$ARG" == "HTTPS" ]; then

	API_URL=$HTTPS_API
        FRONT_URL=$HTTPS_FE
        ASSETS_URL=$HTTPS_ASSETS
	LANDING_URL=$HTTPS_LANDING
	ADMIN_URL=$HTTPS_ADMIN
else
        API_URL=$SSH_API
        FRONT_URL=$SSH_FE
        ASSETS_URL=$SSH_ASSETS
	LANDING_URL=$SSH_LANDING
	ADMIN_URL=$SSH_ADMIN
fi


function _run() {
	
	_write_hosts;
#	_fetch_code;
#	_mount_code;
	_build_and_run;
#	_run_dockerui;	

}

function _write_hosts() {

	if [ "$DEV_IN_HOSTS" == "$DEV_ADDRESS" ]; then
		echo "Writing hosts......"
  		sudo echo "$DEV_ADDRESS  api.pricepilot.loc" >> /etc/hosts
                sudo echo "$DEV_ADDRESS  auth.pricepilot.loc" >> /etc/hosts
                sudo echo "$DEV_ADDRESS  assets.pricepilot.loc" >> /etc/hosts
                sudo echo "$DEV_ADDRESS  new.pricepilot.loc" >> /etc/hosts
	
	else
		echo "Hosts alreaddy written.... "
	fi

}


function _fetch_code() {


	mkdir -p $CUR_DIR/$MAPI
        mkdir -p $CUR_DIR/$MFE
        mkdir -p $CUR_DIR/$MASSETS
        mkdir -p $CUR_DIR/$MLAND
        mkdir -p $CUR_DIR/$MADMIN
	
	if [ -d $HOME/code  ]; then
		rm -rf $HOME/code
		git clone $API_URL $CUR_DIR/$MAPI 
		git clone $FRONT_URL $CUR_DIR/$MFE 
		git clone $ASSETS_URL $CUR_DIR/$MASSETS 
		git clone $LANDING_URL $CUR_DIR/$MLAND 
		git clone $ADMIN_URL  $CUR_DIR/$MADMIN 
        else 
         	 git clone $API_URL $CUR_DIR/$MAPI
                git clone $FRONT_URL $CUR_DIR/$MFE
                git clone $ASSETS_URL $CUR_DIR/$MASSETS
                git clone $LANDING_URL $CUR_DIR/$MLAND
                git clone $ADMIN_URL  $CUR_DIR/$MADMIN

	fi
}

MNT=`mount | grep bind | cut -d' ' -f3`

DIR=$CUR_DIR/containers/production/api_app/repos/pp-api


function _mount_code() {

		mkdir -p $CUR_DIR/$MAPI
		mkdir -p $CUR_DIR/$MFE
		mkdir -p $CUR_DIR/$MASSETS
		mkdir -p $CUR_DIR/$MLAND
		mkdir -p $CUR_DIR/$MADMIN
		echo "created $CUR_DIR/$MAPI, $CUR_DIR/$MFE "

	if  [ "$MNT" = "$MNT | grep prod" ]; then
		echo "Already mounted $MNT"
		sudo umount $CUR_DIR/$MAPI
		sudo umount $CUR_DIR/$MFE
		sudo umount $CUR_DIR/$MASSETS
		sudo umount $CUR_DIR/$MLAND
		sudo umount $CUR_DIR/$MADMIN 
	else
		echo "is  $MNT "
                sudo mount -o bind $HOME/code/api 	 	 $CUR_DIR/$MAPI ; sleep 1
                sudo mount -o bind $HOME/code/frontend		 $CUR_DIR/$MFE; sleep 1
                sudo mount -o bind $HOME/code/assets		 $CUR_DIR/$MASSETS; sleep 1
		sudo mount -o bind $HOME/code/landing		 $CUR_DIR/$MLAND; sleep 1
		sudo mount -o bind $HOME/code/admin		 $CUR_DIR/$MADMIN
	fi
}

P9K=`netstat -an | grep 9000`

function _run_dockerui() {

	$DOCKERUI

}


function _build_and_run() {
	echo $CUR_DIR

	cd $CUR_DIR; docker-compose build --no-cache
		
}

_run

exit 0

