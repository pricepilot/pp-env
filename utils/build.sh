#!/bin/bash

STATUS="/tmp/dkbuild1.lock"
STATUS2="/tmp/dkbuild2.lock"
STATUSTMP="/tmp/dktmp.lock"
BUILD="git@bitbucket.org:pricepilot/pp-env.git"
ENV1="/home/prod/pp-env"
ENV2="/home/prod/pp-env2"
ENVT="$HOME/tmp/build"
MLAND="containers/production/api_app/repos/landing"
MAPI="containers/production/api_app/repos/pp-api"
MFE="containers/production/frontend_app/repos/pp-frontend-new"
MASSETS="containers/production/api_app/repos/assets"

function _main() {

	if [ -f $STATUS2 ]; then
#        	 sudo umount $ENV1/$MAPI
		if [ $? -ne 0  ]; then
			echo "Not unmounted"
		fi
#                sudo umount $ENV1/$MFE
#                sudo umount $ENV1/$MASSETS
#                sudo umount $ENV1/$MLAND
		_build_first;
        	 touch $STATUS
                 rm -rf $STATUS2
	else
		if [ -f $STATUS ]; then
#			sudo umount $ENV2/$MAPI
#	                sudo umount $ENV2/$MFE
#	                sudo umount $ENV2/$MASSETS
#	                sudo umount $ENV2/$MLAND

			_build_second;
			touch $STATUS2
                        rm -rf $STATUS
		fi
		echo "done";
	fi

}	

function _build_tmp() {
	
	if [ -d $ENVT  ]; then
		rm -rf $ENVT 
		git clone $BUILD  $ENVT
		cd $ENVT; ./prod.sh
		if [ $? -ne 0 ]; then
			echo "Not switching, there are Errrorrrrs !!!!!!!!!!!!!!!!!!!"
		fi			
	else
		git clone $BUILD $ENVT 
		cd $ENVT; ./prod.sh 
	fi	
	
}

function _build_first() {
	 if [ -d $ENV1  ]; then
                rm -rf $ENV1
                git clone $BUILD  $ENV1
                cd $ENV1; ./prod.sh
                if [ $? -ne 0 ]; then
                        echo "Not switching, there are Errrorrrrs !!!!!!!!!!!!!!!!!!! \n Check manually PP-ENV"
                else
			cd $ENV2; docker-compose stop
			cd $ENV1; docker-compose up -d 
			if [ $? -ne 0 ]; then
				echo "FAILURE starting First env !!!!!"
			else
				echo "Running fine ....."
			fi
                fi
        else
                git clone $BUILD $ENV1 
                cd $ENV1; ./prod.sh 
		if [ $? -ne 0 ]; then
                        echo "Not switching, there are Errrorrrrs !!!!!!!!!!!!!!!!!!! \n Check manually PP-ENV"
                else
                        cd $ENV2; docker-compose stop
                        cd $ENV1; docker-compose up -d 
                        if [ $? -ne 0 ]; then
                                echo "FAILURE starting First env !!!!!"
                        else
                                echo "Running fine ....."
                        fi
                fi

        fi      

}

function _build_second() {
	if [ -d $ENV2  ]; then
                rm -rf $ENV2
                git clone $BUILD  $ENV2
                cd $ENV2; ./prod.sh
                if [ $? -ne 0 ]; then
                        echo "Not switching, there are Errrorrrrs !!!!!!!!!!!!!!!!!!! \n Check manually PP-ENV2"
                else
                        cd $ENV1; docker-compose stop
                        cd $ENV2; docker-compose up -d
                        if [ $? -ne 0 ]; then
                                echo "FAILURE starting Second env !!!!!"
                        else
                                echo "Running fine ....."
                        fi
                fi
        else
                git clone $BUILD $ENV2
                cd $ENV2; ./prod.sh
		if [ $? -ne 0 ]; then
                        echo "Not switching, there are Errrorrrrs !!!!!!!!!!!!!!!!!!! \n Check manually PP-ENV2"
                else
                        cd $ENV1; docker-compose stop
                        cd $ENV2; docker-compose up -d 
                        if [ $? -ne 0 ]; then
                                echo "FAILURE starting Second env !!!!!"
                        else
                                echo "Running fine ....."
                        fi
                fi
        fi
}

_main

exit 0


