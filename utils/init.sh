#!/bin/bash

ADD_REPO=`sudo sh -c "echo deb https://get.docker.com/ubuntu docker main > /etc/apt/sources.list.d/docker.list" `
UPDATE="sudo apt-get update" 
INSTALL_D="sudo apt-get -y install lxc-docker "
#cgroup-lite lxc"
IMPORT_K=`sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9`
FIG_DL=" sudo wget https://github.com/docker/fig/releases/download/1.0.1/fig-Linux-x86_64 -A /usr/local/bin/fig "
FIG_PERM="sudo chmod +x /usr/local/bin/fig"
FIG_CP="sudo cp fig-Linux-x86_64 /usr/local/bin/fig" 
USER=`whoami`
MOD=`which usermod`
#prephase



#function _main() {
#
#	_revert;
#
#}

function _revert() {
	
	sudo aptitude -y remove lxc-docker cgroup-lite lxc 
        sudo rm -f /usr/local/bin/fig	
	return 0;
}


echo "adding repositories for LXC-docker "
$UPDATE
echo "............."

if [ -e /etc/apt/sources.list.d/docker.list ];then
	echo "docker sources alreaddy added"
else 
	$IMPORT_K
	$ADD_REPO
fi

echo "Installing LXC-docker"
$INSTALL_D

echo "Docker Installed"


if [ -e "$MOD" ]; then 
    sudo usermod -a -G docker $USER
    echo "User $USER  added to Docker group"
fi

echo "Installing FIG.sh"
if [ -e /usr/local/bin/fig ]
    then
	echo "Fig Installed"
    else 
	$FIG_DL
	$FIG_CP	
	$FIG_PERM
fi

#_main 

exit 0
source /etc/profile
