#!/bin/bash


CUR_DIR=`pwd`
E=`cat running-*`
STATUS="$HOME/running-ppenv.lock"
STATUS2="$HOME/running-ppenv2.lock"
BUILD="git@bitbucket.org:pricepilot/pp-env.git"
ENV1="$HOME/pp-env"
ENV2="$HOME/pp-env2"
ENV1TXT="pp-env"
ENV2TXT="pp-env2"
ENVT="$HOME/tmp/build"
MLAND="$CUR_DIR/$E/landing"
MAPI="$CUR_DIR/$E/api"
MFE="$CUR_DIR/$E/frontend"
MASSETS="$CUR_DIR/$E/assets"
ADMIN="$CUR_DIR/$E/admin"



function _main() {
	if [ ! -f $STATUS ] &&  [ ! -f $STATUS2 ]; then
		echo $ENV2TXT > $STATUS2 && mkdir $ENV2;
	fi	

#	touch $STATUS; mkdir $ENV2

	if [ -f $STATUS2 ]; then
		_build_first;
        	 echo $ENV1TXT > $STATUS
                 rm -rf $STATUS2
	else
		if [ -f $STATUS ]; then

			_build_second;
			echo $ENV2TXT > $STATUS2
                        rm -rf $STATUS
		fi
		echo "done";
	fi
#	sendmail -vt < /home/prod/mail
}	


_start_services() { 

		    cd $MLAND; docker-compose up -d 
		    if [ $? -ne 0 ]; then
				echo "FAILURE starting Landing_app !!!!!"
			else
				echo "Running fine ....."
			fi
            cd $MAPI; docker-compose up -d  
            if [ $? -ne 0 ]; then
				echo "FAILURE starting API_APP!!!!!"
			else
				echo "Running fine ....."
			fi
            cd $MFE; docker-compose up -d
            if [ $? -ne 0 ]; then
				echo "FAILURE starting Frontend_app !!!!!"
			else
				echo "Running fine ....."
			fi
            cd $MASSETS; docker-compose up -d
            if [ $? -ne 0 ]; then
				echo "FAILURE starting ASSETS_APP !!!!!"
			else
				echo "Running fine ....."
			fi
            cd $ADMIN; docker-compose up -d

			if [ $? -ne 0 ]; then
				echo "FAILURE starting Admin_app!!!!!"
			else
				echo "Running fine ....."
			fi
}

_stop_services() { 

			cd $MLAND; docker-compose stop 
            cd $MAPI; docker-compose stop  
            cd $MFE; docker-compose stop
            cd $MASSETS; docker-compose stop
            cd $ADMIN; docker-compose stop

			if [ $? -ne 0 ]; then
				echo "FAILURE starting First env !!!!!"
			else
				echo "Running fine ....."
		    fi
	
}


function _build_first() {
	 if [ -d $ENV1  ]; then
	        cd $MAPI; docker-compose rm --force api_apache api_apiapp; cd ../	
                rm -rf $ENV1
                git clone $BUILD  $ENV1
                cd $ENV1 && ./prod.sh
                if [ $? -ne 0 ]; then
                        echo "Not switching, there are Errrorrrrs !!!!!!!!!!!!!!!!!!! \n Check manually PP-ENV"
			exit 0;
                else
			cd $ENV2; _stop_services;
			cd $ENV1; _start_services;
			if [ $? -ne 0 ]; then
				echo "FAILURE starting First env !!!!!"
			else
				echo "Running fine ....."
			fi
                fi
        else
                git clone $BUILD $ENV1 
                cd $ENV1 && ./prod.sh 
		if [ $? -ne 0 ]; then
                        echo "Not switching, there are Errrorrrrs !!!!!!!!!!!!!!!!!!! \n Check manually PP-ENV"
			exit 0;
                else
                        cd $ENV2; docker-compose stop
                        cd $ENV1; docker-compose up -d
                        if [ $? -ne 0 ]; then
                                echo "FAILURE starting First env !!!!!"
                        else
                                echo "Running fine ....."
                        fi
                fi

        fi      

}

function _build_second() {
	if [ -d $ENV2  ]; then
		cd $ENV2; docker-compose rm --force apache apiapp; cd ..
                rm -rf $ENV2
                git clone $BUILD  $ENV2
                cd $ENV2 && ./prod.sh
                if [ $? -ne 0 ]; then
                        echo "Not switching, there are Errrorrrrs !!!!!!!!!!!!!!!!!!! \n Check manually PP-ENV2"
			    exit 0;
                else
                        cd $ENV1; docker-compose stop
                        cd $ENV2; docker-compose up -d
                        if [ $? -ne 0 ]; then
                                echo "FAILURE starting Second env !!!!!"
                        else
                                echo "Running fine ....."
                        fi
                fi
        else
                git clone $BUILD $ENV2
                cd $ENV2 && ./prod.sh
		if [ $? -ne 0 ]; then
                        echo "Not switching, there are Errrorrrrs !!!!!!!!!!!!!!!!!!! \n Check manually PP-ENV2"
			exit 0;
                else
                        cd $ENV1; docker-compose stop
                        cd $ENV2; docker-compose up -d 
                        if [ $? -ne 0 ]; then
                                echo "FAILURE starting Second env !!!!!"
                        else
                                echo "Running fine ....."
                        fi
                fi
        fi
}

_main

exit 0


