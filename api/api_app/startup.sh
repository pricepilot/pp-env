#!/bin/bash
set -e
mkdir -p /var/www/app/api/apps/api/cache
mkdir -p /var/www/app/api/apps/api/logs
mkdir -p /var/www/app/api/apps/auth/cache
mkdir -p /var/www/app/api/apps/auth/logs

chmod -R 777 /var/www/app/api/apps/api/cache
chmod -R 777 /var/www/app/api/apps/api/logs
chmod -R 777 /var/www/app/api/apps/auth/cache
chmod -R 777 /var/www/app/api/apps/auth/logs

php apps/api/console cache:clear --env=dev
php apps/api/console cache:clear --env=prod
php apps/api/console assets:install

php apps/auth/console cache:clear --env=dev
php apps/auth/console cache:clear --env=prod

php apps/api/console doctrine:migrations:migrate --no-interaction >> db_log

cd /var/www/app/api/apps/
chown -R 33 api/cache api/logs auth/cache auth/logs

if [ $? -ne 0 ]; then
	exit 0;
fi
