#!/bin/bash
set -e
ls -l /var/www/app/api/apps/api/ >> script_log
ls -l /var/www/app/api/apps/auth/ >> script_log

mkdir -p /var/www/app/api/apps/api/cache
mkdir -p /var/www/app/api/apps/api/logs
mkdir -p /var/www/app/api/apps/auth/cache
mkdir -p /var/www/app/api/apps/auth/logs

ls -l /var/www/app/api/apps/api/ >> script_log
ls -l /var/www/app/api/apps/auth/ >> script_log

echo "`date` executing change permissions" >> script_log
chmod -R 777 /var/www/app/api/apps/api/cache
chmod -R 777 /var/www/app/api/apps/api/logs
chmod -R 777 /var/www/app/api/apps/auth/cache
chmod -R 777 /var/www/app/api/apps/auth/logs

ls -l /var/www/app/api/apps/api/ >> script_log
ls -l /var/www/app/api/apps/auth/ >> script_log
echo "`date` executing clear cache and install assets" >> script_log
php apps/api/console cache:clear --env=dev >> script_log
php apps/api/console cache:clear --env=prod >> script_log
php apps/api/console assets:install >> script_log
echo "Installing Auth assets"
# php apps/auth/console cache:clear --env=dev >> script_log
# php apps/auth/console cache:clear --env=prod >> script_log
rm -fr apps/auth/cache/*

echo "Executing database migrations"
php apps/api/console doctrine:migrations:migrate --no-interaction >> script_log
echo "`date` executing database migrations" >> script_log
if [ $? -ne 0 ]; then
        exit 1;
fi

echo "`date` Changing folder permissions" >> script_log

chmod -R 777 /var/www/app/api/apps/api/cache
chmod -R 777 /var/www/app/api/apps/api/logs
chmod -R 777 /var/www/app/api/apps/auth/cache
chmod -R 777 /var/www/app/api/apps/auth/logs

ls -l /var/www/app/api/apps/api/ >> script_log
ls -l /var/www/app/api/apps/auth/ >> script_log
cd /var/www/app/api/apps/
echo "Chowning cache and logs"
chown -R 33 api/cache api/logs auth/cache auth/logs

echo "`date` Done changing permissions" >> script_log
